from flask import Flask
from secret import get_secret
from deploy import Deploy
import scheduler
import argparse
import time
import os
# import asyncio
from dotenv import load_dotenv
from pathlib import Path

app = Flask(__name__)

secret_number = None
import signal
import sys
import db

d = None
dotenv_path = Path('.env')
load_dotenv(dotenv_path=dotenv_path)

number = os.getenv("SEC")

@app.route("/return_secret_number")
def get_secret_number():
    number = "{'status: failed}"
    with open('.env', 'r') as f:
        number = f.read()
        number = number.replace("\n", "")
        number = number.replace("\'", "\"")

    return number

if __name__ == '__main__':
    app.run(host='0.0.0.0')
