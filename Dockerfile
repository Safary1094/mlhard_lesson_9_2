FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . /app
EXPOSE 5000:5000

CMD ["python3", "-m", "gunicorn", "-p", "/tmp/service.pid", "-c", "gunicorn.conf", "--workers", "1", "--threads", "5", "--bind", "0.0.0.0:5000", "main:app"]

