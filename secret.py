import requests
import json

URI = 'http://127.0.0.1:5555/get_secret_number/test'

def get_secret():
    while 1:
        r = requests.get(URI)
        if r.status_code != 200:
            json_acceptable_string = r.text.replace("'", "\"")
            d = json.loads(json_acceptable_string)
            if d['status'] == 'Попробуйте позже':
                continue
            else:
                return r.text
        d = r.json()
        return {"secret_number": d['secret_number']}


if __name__ == '__main__':
    print(get_secret())
